﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public static float timeAlive;
    public Text timeAliveText;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {
        timeAliveText.text = "Time Alive: " + string.Format("{00:00:00}", timeAlive);
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1);
        timeAlive++;
        StartCoroutine(Timer());
    }
}
