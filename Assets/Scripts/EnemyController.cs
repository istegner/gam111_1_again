﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float stoppingDistance;
    NavMeshAgent agent;
    public Transform player;
    public int health;
    public int scoreAdd;
    public GameObject gameManager;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        var distance = Vector3.Distance(transform.position, player.position);

        if (distance > stoppingDistance)
        {
            agent.SetDestination(player.position);
        }

        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
