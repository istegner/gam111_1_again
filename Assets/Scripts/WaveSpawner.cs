﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class WaveSpawner : MonoBehaviour
{
    public static int enemiesAlive = 0;

    public Wave[] waves; //The waves
    public Transform[] spawnPoints;

    public float timeBetweenWaves; //Time between waves
    private float countdown; //Time until next wave

    private int waveNumber; //The wave number

    public int difficulty;

    //public Text waveCountdownText; //The UI element to display the time unil the next wave
    //public Text waveNumberText; //The UI element to display the time until the next wave

    // Use this for initialization
    void Start()
    {
        var option = File.ReadAllText(Path.Combine(Application.persistentDataPath, "difficulty.txt"));
        if (option == "1")
        {
            difficulty = 1;
        }
        else if (option == "2")
        {
            difficulty = 2;
        }
        else if(option == "3")
        {
            difficulty = 3;
        }
        Debug.Log("Difficulty: " + option);
    }

    // Update is called once per frame
    void Update()
    {
        //waveNumberText.text = "Wave " + waveNumber;
        //Debug.Log(enemiesAlive);
        //If there are still enemies, do nothing
        //if (enemiesAlive > 0)
        //{
        //    return;
        //}

        //If the countdown until the next wave is over, start the next wave and reset the timer
        if (countdown <= 0)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }

        countdown -= Time.deltaTime;
        //Debug.Log("Wave " + waveNumber + 1 + " in: " + countdown);
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        //Print the time until the next wave in the format 0:00.00
        //waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    /// <summary>
    /// Start the next wave
    /// </summary>
    IEnumerator SpawnWave()
    {
        if(waveNumber >= waves.Length)
        {
            waveNumber = waves.Length;
        }

        Wave wave = waves[waveNumber];
        for (int i = 0; i < wave.count * difficulty; i++)
        {
            //Debug.Log("Spawning enemy " + i);
            SpawnEnemy(wave.enemies);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        waveNumber++;
    }

    //Spawn a random enemy
    void SpawnEnemy(GameObject[] enemy)
    {
        Instantiate(enemy[Random.Range(0, enemy.Length)], spawnPoints[Random.Range(0,spawnPoints.Length)].position, spawnPoints[Random.Range(0, spawnPoints.Length)].rotation);
        enemiesAlive++;
    }
}
