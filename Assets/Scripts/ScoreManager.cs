﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    public int playerScore;

    public Text scoreText;
    
    public string scoreListFileName;
    string fullPath;
    public ScoreList scoreList = new ScoreList();

    void Start()
    {
        //The full path of the high score list (including the file)
        fullPath = System.IO.Path.Combine(Application.persistentDataPath, scoreListFileName);
        //Check if the file exists, if not, create it
        if (!System.IO.File.Exists(fullPath))
        {
            System.IO.File.Create(fullPath);
        }
        //If this is level 1, reset the score
        if (SceneManager.GetActiveScene().name == "Level1")
        {
            playerScore = 0;
        }
        LoadNow();
    }

    void Update()
    {
        scoreText.text = "Score: " + playerScore.ToString();
    }

    public void AddScore(int score)
    {
        Debug.Log("Adding " + score + " score");
        playerScore += score;
    }

    //Save the scorelist
    [ContextMenu("Save")]
    public void SaveNow()
    {
        scoreList.Save(fullPath);
    }

    //Load the scorelist
    [ContextMenu("Load")]
    public void LoadNow()
    {
        scoreList.Load(fullPath);
    }

    //Trim the score list to 10 and sort it
    [ContextMenu("Trim")]
    public void Trim()
    {
        scoreList.SortAndTrim();
    }
}
