﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    Rigidbody rb;

    public float speed;

    public int damage;

    public GameObject player;
    public GameObject gameManager;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 60.0f);
        rb.velocity = transform.forward * speed;
        //Debug.Log("Bullet moving at " + speed + " units");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Damaging " + collision.gameObject.name + " with " + damage + " damage");
            gameManager.GetComponent<ScoreManager>().AddScore(collision.gameObject.GetComponent<EnemyController>().scoreAdd);
            collision.gameObject.GetComponent<EnemyController>().health -= damage;
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }
}
