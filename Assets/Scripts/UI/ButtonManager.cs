using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public Text gameNameText;
    public Text versionText;
    public Dropdown inputChoice;
    public bool useMouse;
    public string settingsFullPath;
    public string difficultyFullPath;
    public string settingsFile;
    public string difficultyFile;

    public GameObject pauseMenu;
    public bool gameIsPaused;

    // Use this for initialization
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            gameNameText.text = Application.productName;
            versionText.text = "V: " + Application.version;

            //The full path including the file
            settingsFullPath = Path.Combine(Application.persistentDataPath, settingsFile);
            //Check if required files exists, if not, create them
            if (!File.Exists(settingsFullPath))
            {
                File.Create(settingsFullPath);
            }
            else
            {
                var option = File.ReadAllText(settingsFullPath);
                if (option == 0.ToString())
                {
                    inputChoice.value = 0;
                }
                else
                {
                    inputChoice.value = 1;
                }
            }
            difficultyFullPath = Path.Combine(Application.persistentDataPath, difficultyFile);
            if (!File.Exists(difficultyFullPath))
            {
                File.Create(difficultyFullPath);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7"))
        {
            if (gameIsPaused)
            {
                ResumeButton();
            }
            else
            {
                PauseGame();
            }
            gameIsPaused = !gameIsPaused;
        }
    }

    public void PlayButton_EASY()
    {
        File.WriteAllText(difficultyFullPath, "1");
        SceneManager.LoadScene("Level1");
    }

    public void PlayButton_MEDIUM()
    {
        File.WriteAllText(difficultyFullPath, "2");
        SceneManager.LoadScene("Level1");
    }

    public void PlayButton_HARD()
    {
        File.WriteAllText(difficultyFullPath, "3");
        SceneManager.LoadScene("Level1");
    }

    public void ExitButton()
    {
        Application.Quit();
        Debug.Log("QUIT");
    }

    public void InputChanged()
    {
        if (inputChoice.value == 0)
        {
            File.WriteAllText(settingsFullPath, inputChoice.value.ToString());
            useMouse = true;
        }
        else
        {
            File.WriteAllText(settingsFullPath, inputChoice.value.ToString());
            useMouse = false;
        }
        Debug.Log("Use mouse? " + useMouse);
    }

    public void MenuPressed()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeButton()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void RestartButton()
    {
        SceneManager.LoadScene("Level1");
        Time.timeScale = 1f;
    }
}
