﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekingBulletScript : MonoBehaviour
{
    Rigidbody rb;

    public Transform target;

    public float speed;

    public int damage;

    public GameObject player;
    public GameObject gameManager;

    // Use this for initialization
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Enemy").transform;
        damage = player.GetComponent<PlayerController>().damage;
    }

    // Update is called once per frame
    void Update()
    {
        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;

        // Move our position a step closer to the target.
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Damaging " + collision.gameObject.name + " with " + damage + " damage");
            gameManager.GetComponent<ScoreManager>().AddScore(collision.gameObject.GetComponent<EnemyController>().scoreAdd);
            collision.gameObject.GetComponent<EnemyController>().health -= damage;
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }
}
