﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Player Stats")]
    public float speed;
    public float normalSpeed;
    public float boostedSpeed;
    public float speedBoostDelay;

    public int damage;
    public int normalDamage;
    public int boostedDamage;
    public int damageBoostDelay;

    protected float hInput = 0f;
    protected float zInput = 0f;
    public int health;
    public Text healthText;
    float xFire;
    float yFire;
    public float shootSensitivity;

    [Header("Bullet Stuff")]
    public GameObject normalBullet;
    public GameObject seekingBullet;
    public Transform bulletSpawn_Right;
    public Transform bulletSpawn_Left;
    public Transform bulletSpawn_Up;
    public Transform bulletSpawn_Down;
    public bool canShoot = true;
    public float shootDelay;
    [Tooltip("0 - Normal, 1 - Seeking, 2 - Full auto")]
    public int gun = 0;

    [Header("Input Mode")]
    public bool useMouse;

    [Header("UI Stuff")]
    public GameObject speedBoostText;
    public GameObject damageBoostText;
    public Text gunModeText;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Player controller script start");
        damage = normalDamage;
        speed = normalSpeed;

        healthText.text = "Health: " + health.ToString();
        //Get the the input mode (controller/mouse & keyboard)
        var option = File.ReadAllText(Path.Combine(Application.persistentDataPath, "settings.txt"));
        if (option == "0")
        {
            useMouse = true;
        }
        else
        {
            useMouse = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Mouse & keyboard input for shooting
        if (useMouse)
        {
            if (gun == 0 || gun == 1)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    Shoot(bulletSpawn_Left);
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    Shoot(bulletSpawn_Right);
                }
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    Shoot(bulletSpawn_Up);
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    Shoot(bulletSpawn_Down);
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    Shoot(bulletSpawn_Left);
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    Shoot(bulletSpawn_Right);
                }
                else if (Input.GetKey(KeyCode.UpArrow))
                {
                    Shoot(bulletSpawn_Up);
                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    Shoot(bulletSpawn_Down);
                }
            }
        }
        //Controller input for shooting
        else
        {
            xFire = Input.GetAxis("xShoot");
            yFire = Input.GetAxis("yShoot");

            if (xFire > shootSensitivity && canShoot)
            {
                canShoot = false;
                Shoot(bulletSpawn_Left);
                StartCoroutine(ShootReset());
            }
            else if (xFire < -shootSensitivity && canShoot)
            {
                canShoot = false;
                Shoot(bulletSpawn_Right);
                StartCoroutine(ShootReset());
            }
            else if (yFire > shootSensitivity && canShoot)
            {
                canShoot = false;
                Shoot(bulletSpawn_Up);
                StartCoroutine(ShootReset());
            }
            else if (yFire < -shootSensitivity && canShoot)
            {
                canShoot = false;
                Shoot(bulletSpawn_Down);
                StartCoroutine(ShootReset());
            }
        }

        if (health <= 0)
        {
            Die();
        }
    }

    //Player movement
    private void FixedUpdate()
    {
        hInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");

        Vector3 translation = new Vector3(hInput, 0, zInput);
        transform.Translate(translation * speed * Time.deltaTime);
    }

    /// <summary>
    /// Shoot in direction "direction"
    /// </summary>
    /// <param name="direction"></param>
    void Shoot(Transform direction)
    {
        Debug.Log("Shooting bullet with " + damage + " damage");
        if (gun == 0)
        {
            var bullet  = (GameObject)Instantiate(normalBullet, direction.position, direction.rotation);
            bullet.GetComponent<BulletController>().damage = damage;
        }
        else if (gun == 1)
        {
            var bullet = (GameObject)Instantiate(seekingBullet, direction.position, direction.rotation);
            bullet.GetComponent<BulletController>().damage = damage;
        }
    }

    /// <summary>
    /// If the player collides with an enemy, destroy said enemy and take 1 health from the player
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            health--;
            healthText.text = "Health: " + health.ToString();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SpeedBoost"))
        {
            Destroy(other.gameObject);
            speedBoostText.SetActive(true);
            speed = boostedSpeed;
            StartCoroutine(SpeedReset());
        }

        if (other.gameObject.CompareTag("DamageBoost"))
        {
            Destroy(other.gameObject);
            damageBoostText.SetActive(true);
            damage = boostedDamage;
            StartCoroutine(DamageReset());
        }

        if (other.gameObject.CompareTag("SeekingGun"))
        {
            Destroy(other.gameObject);
            gunModeText.text = "SEEKING BULLETS";
            gun = 1;
        }

        if (other.gameObject.CompareTag("FullAutoGun"))
        {
            Destroy(other.gameObject);
            gunModeText.text = "FULL AUTO";
            gun = 2;
        }
    }

    //Go back to the main menu upon death
    void Die()
    {
        //TODO: have a death scene
        SceneManager.LoadScene(0);
    }

    //Delay between shots
    IEnumerator ShootReset()
    {
        yield return new WaitForSeconds(shootDelay);
        canShoot = true;
    }

    IEnumerator SpeedReset()
    {
        yield return new WaitForSeconds(speedBoostDelay);
        speed = normalSpeed;
        speedBoostText.SetActive(false);
    }

    IEnumerator DamageReset()
    {
        yield return new WaitForSeconds(damageBoostDelay);
        damage = normalDamage;
        damageBoostText.SetActive(false);
    }
}
